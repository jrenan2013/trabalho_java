public class ListaDePessoas {
	public static void main(String [] arg){
		try{
			Pessoa [] pessoas = new Pessoa[]{
				new Pessoa(1, "JoseRenan"),
				new Pessoa(2, "Dell"),
				new Pessoa(3, "Arley"),
				new Pessoa(4, "Charley"),
				new Pessoa(5, "Hiago"),
				new Pessoa(6, "Thales"),
				new Pessoa(6, "Thiago")
			};
			for(Pessoa item: pessoas){
				System.out.println(item.toString());
			}
		}catch(Exception ex){
			System.out.println(ex);
		}
	}
}
