public class Pessoa {
	//Declarar atributos (propriedades)
	int ident;
	
	String nome;
	//Construtor
	//que altera algum atributo do objeto
	public Pessoa(int ID, String nome)throws Exception{
		setIdent(ID);
		setNome(nome);
	}
	//Métodos
	// que retorna valor (boolean)

	public static boolean validarNome(String novo){
		boolean resultado=false;
		if(novo.length() > 2){
			resultado = true;
		}else if(novo.toUpperCase()==novo.toString()){
			
			if(novo.length() > 1){
				if(novo.length() > 1){
					resultado = true;
				}
			}
		}
		return resultado;
	}
	//Metodos set e get
	public String getNome(){
		return nome;
	}
	public int getIdent(){
		return ident;
	}
	//Método protect
	//que lança exceção
	protected void setNome(String novo)throws Exception{
		if(validarNome(novo)==true){
			this.nome = novo;
		}else {
			throw new NomeInvalidoException();
		}
	}
	// private
	private void setCodigo(int ID){
		this.ident = ID;
	}
	public String toString(){
		return getIdent() + "; " + getNome();
	}
}
//Uma exceção personalizada
class NomeInvalidoException extends Exception {
	public String toString(){
		return "Nome inválido!";
	}
}
